<?php

namespace App\Requests;

use Slim\Psr7\Request;

/**
 * Class CustomHandlerRequest
 *
 * @package App\Requests
 */
class CustomHandlerRequest
{
	/**
	 * @param \Slim\Psr7\Request $request
	 * @param $key
	 * @param null $default
	 * @return mixed|null
	 */
	public static function getParam(Request $request, $key, $default = null)
	{
		$postParams = $request->getParsedBody();
		$getParams = $request->getQueryParams();
		$getBody = json_decode($request->getBody(), true);

		$result = $default;

		if (is_array($postParams) && isset($postParams[$key]))
		{
			$result = $postParams[$key];
		}
		elseif (is_object($postParams) && property_exists($postParams, $key))
		{
			$result = $postParams->$key;
		}
		elseif (is_array($getBody) && isset($getBody[$key]))
		{
			$result = $getBody[$key];
		}
		elseif (isset($getParams[$key]))
		{
			$result = $getParams[$key];
		}

		return $result;
	}
}