<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Categories
 *
 * @package App\Model
 */
class Categories extends Model
{
	/**
	 * @var string
	 */
	protected $table    = 'categories';

	/**
	 * @var array
	 */
	protected $fillable = ['parent_id', 'label', 'status'];
}