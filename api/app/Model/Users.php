<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Users
 *
 * @package App\Model
 */
class Users extends Model
{
	/**
	 * @var string
	 */
	protected $table    = 'users';

	/**
	 * @var array
	 */
	protected $fillable = ['email', 'password'];

}