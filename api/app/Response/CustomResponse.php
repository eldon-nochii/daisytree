<?php

namespace App\Response;

use Slim\Psr7\Response;

/**
 * Class CustomResponse
 *
 * @package App\Response
 */
class CustomResponse
{
	/**
	 * @param \Slim\Psr7\Response $response
	 * @param $message
	 * @param bool $response_only
	 * @return \Slim\Psr7\Response
	 */
	public function is200(Response $response, $message, $response_only = false): Response
	{
		if (false == $response_only)
		{
			$message = [
				"success"  => true,
				"response" => $message,
			];
		}

		$response->getBody()->write(json_encode($message));

		return $response->withHeader("Content-Type", "application/json")
			->withStatus(200);
	}

	/**
	 * @param \Slim\Psr7\Response $response
	 * @param $message
	 * @return \Psr\Http\Message\ResponseInterface|\Slim\Psr7\Response
	 */
	public function is400(Response $response, $message): Response
	{
		$message = [
			"success"  => false,
			"response" => $message,
		];
		$response->getBody()->write(json_encode($message));

		return $response->withHeader("Content-Type", "application/json")
			->withStatus(400);
	}
}