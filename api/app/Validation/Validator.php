<?php

namespace App\Validation;

use App\Requests\CustomHandlerRequest;
use Respect\Validation\Exceptions\NestedValidationException;

/**
 * Class Validator
 *
 * @package App\Validation
 */
class Validator
{
	/**
	 * @var array
	 */
	public $errors = [];

	/**
	 * @param $request
	 * @param array $rules
	 * @return $this
	 */
	public function validate($request, array $rules)
	{
		foreach ($rules as $field => $value)
		{
			try
			{
				$value->setName($field)->assert(CustomHandlerRequest::getParam($request, $field));
			}
			catch (NestedValidationException $ex)
			{
				$this->errors[$field] = $ex->getMessages();
			}
		}

		return $this;
	}

	/**
	 * @return bool
	 */
	public function failed(): bool
	{
		return !empty($this->errors);
	}

}