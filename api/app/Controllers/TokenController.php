<?php

namespace App\Controllers;

use Firebase\JWT\JWT;

/**
 * Class TokenController
 *
 * @package App\Controllers
 */
class TokenController
{
	/**
	 * @param $email
	 * @return string
	 */
	public static function generate($email): string
	{
		$now = time();
		$expires = strtotime('+1 hour', $now);
		$secret = getenv('JWT_SECRET_KEY');
		$payload = [
			"jti" => $email,
			"iat" => $now,
			"exp" => $expires,
		];

		return JWT::encode($payload, $secret, "HS256");
	}
}