<?php

namespace App\Controllers;

use App\Model\Categories;
use App\Response\CustomResponse;
use App\Validation\Validator;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Class CategoryController
 *
 * @package App\Controllers
 */
class CategoryController
{
	/**
	 * @var \App\Response\CustomResponse
	 */
	protected $customResponse;

	/**
	 * @var \App\Validation\Validator
	 */
	protected $validator;

	/**
	 * @var \App\Model\Categories
	 */
	protected $categories;

	/**
	 * CategoryController constructor.
	 */
	public function __construct()
	{
		$this->customResponse = new CustomResponse();
		$this->validator = new Validator();
		$this->categories = new Categories();
	}

	/**
	 * @param \Psr\Http\Message\ResponseInterface $response
	 * @param null $id
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function view(Response $response, $id = null): Response
	{
		$categories = $this->categories->get();

		if (null !== $id)
		{
			$categories = $this->categories->where(['id' => $id])->get();
		}

		return $this->customResponse->is200($response, $categories, true);
	}

	/**
	 * @param \Psr\Http\Message\ResponseInterface $response
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function viewTree(Response $response): Response
	{
		$categories = $this->categories->get();

		$new_category = [];
		foreach ($categories as $index => $category)
		{
			$categories[$index] ['name'] = $categories[$index] ['label'];
			unset($categories[$index]['label']);

			$new_category[$category->parent_id][] = $category;
		}
		$tree_category = $this->createTree($new_category, $new_category[0]);

		return $this->customResponse->is200($response, $tree_category, true);
	}

	/**
	 * @param $list
	 * @param $parent
	 * @return array
	 * https://stackoverflow.com/questions/4196157/create-array-tree-from-array-list
	 */
	protected function createTree(&$list, $parent)
	{
		$tree = [];
		foreach ($parent as $k => $l)
		{
			if (isset($list[$l['id']]))
			{
				$l['children'] = $this->createTree($list, $list[$l['id']]);
			}
			$tree[] = $l;
		}

		return $tree;
	}
}