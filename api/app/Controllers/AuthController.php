<?php

namespace App\Controllers;

use App\Model\Users;
use App\Requests\CustomHandlerRequest;
use App\Response\CustomResponse;
use App\Validation\Validator;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Respect\Validation\Validator as validate;

/**
 * Class AuthController
 *
 * @package App\Controllers
 */
class AuthController
{
	/**
	 * @var \App\Validation\Validator
	 */
	protected $validator;

	/**
	 * @var \App\Response\CustomResponse
	 */
	protected $customResponse;

	/**
	 * @var \App\Model\Users
	 */
	protected $user;

	/**
	 * AuthController constructor.
	 */
	public function __construct()
	{
		$this->validator = new Validator();
		$this->customResponse = new CustomResponse();
		$this->user = new Users();
	}

	/**
	 * @param \Psr\Http\Message\RequestInterface $request
	 * @param \Psr\Http\Message\ResponseInterface $response
	 * @return \Psr\Http\Message\ResponseInterface|\Slim\Psr7\Response
	 */
	public function login(Request $request, Response $response): Response
	{
		$this->validator->validate(
			$request,
			[
				"email"    => validate::notEmpty()->email(),
				"password" => validate::notEmpty(),
			]
		);

		if ($this->validator->failed())
		{
			return $this->customResponse->is400($response, $this->validator->errors);
		}

		$verify_account = $this->verify(
			CustomHandlerRequest::getParam($request, 'email'),
			CustomHandlerRequest::getParam($request, 'password')
		);

		if ($verify_account == false)
		{
			return $this->customResponse->is400($response, 'Wrong username or password');
		}

		$generated_token = TokenController::generate(
			CustomHandlerRequest::getParam($request, 'email')
		);

		return $this->customResponse->is200($response, $generated_token);
	}

	/**
	 * @param $email
	 * @param $password
	 * @return bool
	 */
	public function verify($email, $password): bool
	{
		$count = $this->user->where(["email" => $email])->count();
		if ($count == 0)
		{
			return false;
		}

		$user = $this->user->where(["email" => $email])->first();
		$hashed_password = $user->password;

		$verify = password_verify($password, $hashed_password);
		if ($verify == false)
		{
			return false;
		}

		return true;
	}

	/**
	 * @param \Psr\Http\Message\RequestInterface $request
	 * @param \Psr\Http\Message\ResponseInterface $response
	 * @return \Psr\Http\Message\ResponseInterface|\Slim\Psr7\Response
	 */
	public function register(Request $request, Response $response): Response
	{
		$this->validator->validate(
			$request,
			[
				"email"    => validate::notEmpty()->email(),
				"password" => validate::notEmpty(),
			]
		);

		if ($this->validator->failed())
		{
			return $this->customResponse->is400($response, $this->validator->errors);
		}

		if ($this->isEmailExist(CustomHandlerRequest::getParam($request, "email")))
		{
			return $this->customResponse->is400($response, 'Email already existing');
		}

		$password_hash = $this->hashPassword(CustomHandlerRequest::getParam($request, 'password'));

		$this->user->create(
			[
				"email"    => CustomHandlerRequest::getParam($request, "email"),
				"password" => $password_hash,
			]
		);

		return $this->customResponse->is200($response, 'User created successfully');
	}

	/**
	 * @param $email
	 * @return bool
	 */
	public function isEmailExist($email): bool
	{
		$count = $this->user->where(['email' => $email])->count();
		if ($count == 0)
		{
			return false;
		}

		return true;
	}

	/**
	 * @param $password
	 * @return false|string|null
	 */
	public function hashPassword($password)
	{
		return password_hash($password, PASSWORD_DEFAULT);
	}
}