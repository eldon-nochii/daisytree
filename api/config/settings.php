<?php

use Dotenv\Dotenv;
use Psr\Container\ContainerInterface;

return function (ContainerInterface $container) {
	$dotenv = Dotenv::createImmutable('../');
	$dotenv->load();

	$container->set(
		'settings',
		function () {
			$db = require __DIR__ . '/database.php';

			return [
				"db" => $db,
			];
		}
	);
};