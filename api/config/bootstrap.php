<?php

use DI\Bridge\Slim\Bridge;
use DI\Container;

require_once __DIR__ . '/../vendor/autoload.php';

$container = new Container();

$settings = require_once __DIR__ . '/settings.php';
$settings($container);

$app = Bridge::create($container);

$middleware = require_once __DIR__ . '/middleware.php';
$middleware($app);

$routes = require_once __DIR__ . '/routes.php';
$routes($app);

$app->run();