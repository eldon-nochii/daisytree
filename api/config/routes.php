<?php

use App\Controllers\AuthController;
use App\Controllers\CategoryController;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\App;

return function (App $app) {

	$app->group(
		"/category",
		function ($app) {
			$app->get('/list', [CategoryController::class, 'view']);
			$app->get('/list/{id}', [CategoryController::class, 'view']);
			$app->get('/tree', [CategoryController::class, 'viewTree']);
		}
	);

	$app->group(
		"/auth",
		function ($app) {
			$app->post("/register", [AuthController::class, "register"]);
			$app->post("/login", [AuthController::class, "login"]);
			$app->options(
				'/login',
				function (ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
					return $response;
				}
			);
		}
	);

	$app->options(
		'/{routes:.+}',
		function ($request, $response, $args) {
			return $response;
		}
	);

	$app->add(
		function ($request, $handler) {
			$response = $handler->handle($request);

			return $response->withHeader('Access-Control-Allow-Origin', '*')->withHeader(
				'Access-Control-Allow-Headers',
				'X-Requested-With, Content-Type, Accept, Origin, Authorization'
			)->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
		}
	);
};