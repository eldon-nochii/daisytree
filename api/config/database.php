<?php

$db_config = [
	'driver'    => getenv('DB_CONNECTION'),
	'host'      => getenv('DB_HOST'),
	'database'  => getenv('DB_DATABASE'),
	'username'  => getenv('DB_USERNAME'),
	'password'  => getenv('DB_PASSWORD'),
	'charset'   => getenv('DB_CHARSET'),
	'collation' => getenv('DB_COLLATION'),
	'prefix'    => getenv('DB_PREFIX'),
];

$capsule = new Illuminate\Database\Capsule\Manager;
$capsule->addConnection($db_config);
$capsule->setAsGlobal();
$capsule->bootEloquent();

return $capsule;