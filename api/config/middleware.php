<?php

use Slim\App;
use Tuupola\Middleware\JwtAuthentication;

return function (App $app) {
	$app->getContainer()->get('settings');
	$app->addRoutingMiddleware();
	$app->add(
		new JwtAuthentication(
			[
				"ignore" => ["/auth/login", "/auth/register"],
				"secret" => getenv('JWT_SECRET_KEY'),
				"error"  => function ($response, $arguments) {
					$data["success"] = false;
					$data["response"] = $arguments["message"];
					$data["status_code"] = "401";

					return $response
						->withHeader("Content-type", "application/json")
						->getBody()->write(
							json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT)
						);
				},
			]
		)
	);
	$app->addErrorMiddleware(true, true, true);
};