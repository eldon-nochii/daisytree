export class Category {
  id!: number;
  parent_id!: number;
  label!: string;
  status!: number;
}
