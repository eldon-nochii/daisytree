import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {Category} from 'src/app/model/category';
import {User} from 'src/app/model/user';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  API_SERVER = "http://localhost:8080";

  constructor(private httpClient: HttpClient) {
  }

  viewCategories(token: any): Observable<Category[]> {
    let headers: HttpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    });
    return this.httpClient.get<Category[]>(`${this.API_SERVER}/category/tree`, {headers: headers}).pipe(
      map((data: Category[]) => {
        return data;
      }), catchError(error => {
        return throwError(error.error.response);
      }),
    );
  }

  loginUser(user: User): Observable<any> {
    let headers: HttpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.httpClient.post(`${this.API_SERVER}/auth/login`, user, {headers: headers}).pipe(
      map((data: any) => {
        return data.response;
      }), catchError(error => {
        return throwError(error.error.response);
      }),
    );
  }
}
