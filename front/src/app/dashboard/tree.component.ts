import {Component, OnDestroy, OnInit} from '@angular/core';
import {ITreeOptions} from '@circlon/angular-tree-component';
import {Observable, Subscription} from 'rxjs';
import {finalize, map, switchMap} from 'rxjs/operators';
import {ApiService} from 'src/app/api/api.service';
import {Category} from 'src/app/model/category';
import {User} from 'src/app/model/user';

@Component({
  selector: 'app-checkboxes',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.css'],
})
export class TreeComponent implements OnInit, OnDestroy {
  userLogin: User = {
    id: 0,
    email: "",
    password: "",
  };
  options: ITreeOptions = {
    useCheckbox: true,
  };
  nodes: Category[] = [];
  token: any;
  dataSubscription!: Subscription;

  constructor(private apiService: ApiService) {
  }

  ngOnInit(): void {
    this.dataSubscription = this.loadCategories().pipe(
      finalize(() => this.dataSubscription.unsubscribe()),
    ).subscribe();
  }

  loadCategories(): Observable<any> {
    return this.apiService.viewCategories(this.token).pipe(
      map((nodes) => {
        this.nodes = nodes;
      }),
    );
  }

  login(form: any) {
    this.dataSubscription = this.apiService.loginUser(form.value).pipe(
      switchMap((token: any) => {
        this.token = token;
        return this.loadCategories();
      }),
      finalize(() => this.dataSubscription.unsubscribe()),
    ).subscribe();
  }

  ngOnDestroy(): void {
    this.dataSubscription.unsubscribe();
  }

}
