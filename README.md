# Daisy Tree #

This small app uses slim 4 framework and angular 9 (assessment)

![daisytree](api/public/daisytree.gif)

### Here are all the reference use for the entiry of this app ###

* https://www.slimframework.com/docs/v4/start/installation.html
* https://github.com/tuupola/slim-basic-auth
* https://github.com/zhorton34/authorize-slim-4
* https://www.youtube.com/watch?v=AJb1ibC4gj0
* https://stackoverflow.com/questions/4196157/create-array-tree-from-array-list
* https://angular2-tree.readme.io/
* https://www.npmjs.com/package/@widgetjs/tree
* https://www.techiediaries.com/angular/php-angular-9-crud-api-httpclient/

### Backend installation ###

* Under api folder just run composer install
* Rename the .env.example to .env and provide your database details
* Import the database file from /config/daisytree.sql
* Once done, run the server php -S localhost:8080 -t public

Take note of the URL and port number use, as it will be use in this file of the frontend too
front/src/app/api/api.service.ts:12

### Working endpoints ###

* /category/tree (formatted-list)
* /category/list (list)
* /category/list/id (specific)
* /auth/login (obtain token)
* /auth/register (add user)

### Frontend installation ###

* Under front folder run npm install
* Start the server by running ng server
* Then you should be able to access http://localhost:4200/

### Missing/not done ###

* Frontend app is not showing errors in the page, only in console.log
* No general styling applied 

### How it works ###

* The categories will be fetch via api provided if there is a token or authenticated
* You can register an account via the register endpoint using any api platform like postman
* Once login is sucessful, it will show the tree

### Concerns/feedback ###
* Should there be any issues, feel free to email me at eldon@codingchiefs.com